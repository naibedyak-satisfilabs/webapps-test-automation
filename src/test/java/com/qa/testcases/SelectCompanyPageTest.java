package com.qa.testcases;



import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.qa.base.TestBase;

import com.qa.pages.SatisfiLoginPage;
import com.qa.pages.SelectCompanyPage;
import com.qa.util.TestUtil;

public class SelectCompanyPageTest extends TestBase {
	
	SatisfiLoginPage loginPage; //Login page object reference. It will be defined at class level to be used throughout the program.
	SelectCompanyPage selectCompanyPage;
	//SatisfiInboxPage inboxPage;
	
	
	public SelectCompanyPageTest(){
		super(); 
	}
	
	@Parameters({"browser", "appEnvironment"})
	@BeforeMethod(alwaysRun = true)
	public void setUp(String Browser, String Env){
		reportLog("Login Page class - Before method execution");
		initialization(Browser, Env);
		loginPage = new SatisfiLoginPage(); //Create object of SatisfiLoginPage() class. This is used to access all the functions/methods of SatisfiLoginPage() class
		
	}
	
 /*@Test(priority=7, groups = {"PRIME-T209"}, description = "TC_Login_07")
	public void verify_UserName_Test(){
		reportLog("Validating user name displayed in dashboard page");
		Assert.assertTrue(selectCompanyPage.verifyCorrectUserNameByName(prop.getProperty("selectCompanyPagePageUserName")));
	}*/
	
	@Test(priority=1, groups = {"PRIME-T210"}, description="TC_CompanySelection_01")
	public void login_Company_Page_Test() throws Exception{
		reportLog("Login to Satisfi Labs app");
		
		//this login() method is returning an object of HomePage class. So I stored this in dashboardPage class reference object
		reportLog("Login using username as: "+prop.getProperty("username")+" and password as: "+prop.getProperty("password"));
		
		//Data Driven example for login function
		//selectCompanyPage = loginPage.login(userName, password);
		
		//Fetching data from properties file
		selectCompanyPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		
		reportLog("Validating login is successfull by checking dashboard page title");
		String companyPageTitle = selectCompanyPage.verifySelectCompanyPageTitle();
		String CompanySelectPage1 = "https://dashboard.satisfitest.us/";
		Assert.assertEquals(companyPageTitle, CompanySelectPage1,"Select Company Title Not Matched");
		//Sample for color coding the log line
		reportLog("<b><font color='Purple'> Login Verification successfully Completed.:</font><b>");
		
		reportLog("*****Selecting Company*****");
		//TestUtil.threadMethod();
		Thread.sleep(3000);
		reportLog("Clicking on the Select Company Dropdown");
		selectCompanyPage.autoSelectCompany();
		reportLog("Searching the Mindfire QA Test Server 959 Company:");
		selectCompanyPage.searchSelectCompany("959");
		
		selectCompanyPage.selectCompanyOption();
		Thread.sleep(2000);
		reportLog("Validated and Selected the Company and Clicking on Select Button");
		selectCompanyPage.selectButton();
		
		reportLog("Company selected");
		
		reportLog("<b><font color='Purple'> Login Verification successfully Completed, User Navigated to Inbox Page.</font><b>");
		//Sample code on how to take screenshot at a particular step that will be added in Extent report.
		
		reportLog("Logging out of the application.");
		loginPage.logout();
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority = 2, groups = { "PRIME-T210" }, description = "TC_CompanySelection_02")
	public void company_Selection_UI_Elements_Test() throws Exception {
		
		reportLog("Login to Satisfi Labs app");
		reportLog("Login using username as: "+prop.getProperty("username")+" and password as: "+prop.getProperty("password"));
		
		//Fetching data from properties file
		selectCompanyPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		
		reportLog("Validating login is successfull by checking dashboard page title");
		String companyPageTitle = selectCompanyPage.verifySelectCompanyPageTitle();
		String CompanySelectPage1 = "https://dashboard.satisfitest.us/";
		Assert.assertEquals(companyPageTitle, CompanySelectPage1,"Select Company Title Not Matched");
		//Sample for color coding the log line
		reportLog("<b><font color='Purple'> Login Verification successfully Completed.:</font><b>");
		TestUtil.threadMethod(2000);
		reportLog("Validating the various web elements present in Company Selection screen");
		boolean cmpnylabel = selectCompanyPage.cmpynSelectionLabel();
		Assert.assertTrue(cmpnylabel);
		reportLog("Validated the Company Selection Label is displaying in Company Selection screen");
		
		boolean menuBar = selectCompanyPage.cmpynSelectionMenuBar();
		Assert.assertTrue(menuBar);
		reportLog("Validated the Menu Bar is displaying in Company Selection screen");
		
		boolean cmpnyDrop = selectCompanyPage.selectCompanyDrop();
		Assert.assertTrue(cmpnyDrop);
		reportLog("Validated the Company Select Dropdown is displaying in Company Selection screen");
		
		boolean cmpnyBtn = selectCompanyPage.selectCompanyBtn();
		Assert.assertTrue(cmpnyBtn);
		reportLog("Validated the Select Company Button is displaying in Company Selection screen");
		
		boolean newCmpnyLink = selectCompanyPage.createNewCmpnyLink();
		Assert.assertTrue(newCmpnyLink);
		reportLog("Validated the Create New Company Link is displaying in Company Selection screen");
		
		TestUtil.threadMethod(2000);
		selectCompanyPage.clickMenuBar();		//Clicking on the Menu Bar
		
		boolean activeCmpny = selectCompanyPage.showActiveCompanies();
		Assert.assertTrue(activeCmpny);
		reportLog("Validated the Show Active Companies is displaying in Company Selection screen");
		
		boolean inactiveCmpny = selectCompanyPage.includeInactiveCompanies();
		Assert.assertTrue(inactiveCmpny);
		reportLog("Validated the Include Inactive Companies is displaying in Company Selection screen");	
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	@Test(priority = 3, groups = { "PRIME-T210" }, description = "TC_CompanySelection_03")
	public void click_Create_New_Company_Test() throws Exception {
		
		reportLog("Login to Satisfi Labs app");
		reportLog("Login using username as: "+prop.getProperty("username")+" and password as: "+prop.getProperty("password"));
		
		//Fetching data from properties file
		selectCompanyPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		
		reportLog("Validating login is successfull by checking dashboard page title");
		String companyPageTitle = selectCompanyPage.verifySelectCompanyPageTitle();
		String CompanySelectPage1 = "https://dashboard.satisfitest.us/";
		Assert.assertEquals(companyPageTitle, CompanySelectPage1,"Select Company Title Not Matched");
		//Sample for color coding the log line
		reportLog("<b><font color='Purple'> Login Verification successfully Completed.:</font><b>");
		TestUtil.threadMethod(2000);
				
		boolean newCmpnyLink = selectCompanyPage.createNewCmpnyLink();
		Assert.assertTrue(newCmpnyLink);
		reportLog("Validated the Create New Company Link is displaying in Company Selection screen");
		
		selectCompanyPage.clickCreateNewcompany();
		TestUtil.threadMethod(2000);
		selectCompanyPage.createCompanyTextMsg();
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority = 4, groups = { "PRIME-T210" }, description = "TC_CompanySelection_04")
	public void create_New_Company_Modal_UI_Test() throws Exception {
		
		reportLog("Login to Satisfi Labs app");
		reportLog("Login using username as: "+prop.getProperty("username")+" and password as: "+prop.getProperty("password"));
		
		//Fetching data from properties file
		selectCompanyPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		
		reportLog("Validating login is successfull by checking dashboard page title");
		String companyPageTitle = selectCompanyPage.verifySelectCompanyPageTitle();
		String CompanySelectPage1 = "https://dashboard.satisfitest.us/";
		Assert.assertEquals(companyPageTitle, CompanySelectPage1,"Select Company Title Not Matched");
		//Sample for color coding the log line
		reportLog("<b><font color='Purple'> Login Verification successfully Completed.:</font><b>");
		TestUtil.threadMethod(2000);
				
		boolean newCmpnyLink = selectCompanyPage.createNewCmpnyLink();
		Assert.assertTrue(newCmpnyLink);
		reportLog("Validated the Create New Company Link is displaying in Company Selection screen");
		
		selectCompanyPage.clickCreateNewcompany();
		TestUtil.threadMethod(2000);
		
		boolean createCmpnyText = selectCompanyPage.createCompanyTextMsg();
		Assert.assertTrue(createCmpnyText);
		reportLog("Validated the Create Company TextBox is displaying in Company Selection screen");
		TestUtil.threadMethod(2000);
		
		boolean cmpnyname = selectCompanyPage.companyNameTextBox();
		Assert.assertTrue(cmpnyname);
		reportLog("Validated the Company Name TextBox is displaying in Company Selection screen");
		TestUtil.threadMethod(2000);
		
		String initSet = selectCompanyPage.InitializeSettingsText();
		String actualinit = "Initialize Settings From";
		Assert.assertEquals(initSet, actualinit);
		reportLog("<b><font color='Purple'>Validated the </font><b> "+initSet+" message in Create Company Screen.");
		TestUtil.threadMethod(2000);
		
		String defaultopt = selectCompanyPage.selectCompanytemplate();
		String actualDef = "*Default Company Template";
		Assert.assertEquals(defaultopt, actualDef);
		reportLog("<b><font color='Purple'>Validated the </font><b> "+defaultopt+" is selected in Company Screen.");
		TestUtil.threadMethod(2000);
		
		boolean cpySystxt = selectCompanyPage.cpySyssetupText();
		Assert.assertTrue(cpySystxt);
		reportLog("Validated the Copy System Setup is displaying in Company Selection screen");
		
		boolean cpyThemes = selectCompanyPage.cpyThemesText();
		Assert.assertTrue(cpyThemes);
		reportLog("Validated the Copy Themes is displaying in Company Selection screen");
				
		boolean cpyNLC = selectCompanyPage.cpyNLCDBText();
		Assert.assertTrue(cpyNLC);
		reportLog("Validated the Copy NLC DB is displaying in Company Selection screen");
		
		selectCompanyPage.setupsCheckbox();
		Assert.assertTrue(true);
		reportLog("Validated the Checkboxes are selected in Company Selection screen");
		
		selectCompanyPage.setupsCheckboxDisabled();
		Assert.assertFalse(false);
		reportLog("Validated the Checkboxes are Disabled/non-Editable in Company Selection screen");
		
		boolean createBtn = selectCompanyPage.createCompanyBtnTxt();
		Assert.assertTrue(createBtn);
		reportLog("Validated the Create Company button is displaying in Company Selection screen");
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority=5, groups = {"PRIME-T210"}, description="TC_CompanySelection_05")
	public void create_Company_Error_Test() throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Login using username as: "+prop.getProperty("username")+" and password as: "+prop.getProperty("password"));
		
		//Fetching data from properties file
		selectCompanyPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		
		reportLog("Validating login is successfull by checking dashboard page title");
		String companyPageTitle = selectCompanyPage.verifySelectCompanyPageTitle();
		String CompanySelectPage1 = "https://dashboard.satisfitest.us/";
		Assert.assertEquals(companyPageTitle, CompanySelectPage1,"Select Company Title Not Matched");
		//Sample for color coding the log line
		reportLog("<b><font color='Purple'> Login Verification successfully Completed.:</font><b>");
		TestUtil.threadMethod(2000);
				
		boolean newCmpnyLink = selectCompanyPage.createNewCmpnyLink();
		Assert.assertTrue(newCmpnyLink);
		reportLog("Validated the Create New Company Link is displaying in Company Selection screen");
		
		selectCompanyPage.clickCreateNewcompany();
		TestUtil.threadMethod(2000);
		selectCompanyPage.createCompanyBtn();
		TestUtil.threadMethod(2000);
		reportLog("Validating the Error message by clicking on create company buttton without providing company name in Company Selection screen");
		
		String companyErrorMsg = selectCompanyPage.companyError();
		String actualcompanyErrorMsg = "Please enter company name.";
		Assert.assertEquals(companyErrorMsg,actualcompanyErrorMsg);
		reportLog("<b><font color='Purple'>Validated the </font><b> "+companyErrorMsg+" message in Company Selection Screen.");
		
		reportLog("Taking Screenshot of the failed Login below.");
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Error Message Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		reportLog("Take sceenshot in case of failue and close browser");
		//Check if the test case failed or was skipped and take screenshot
		if(result.getStatus()==result.FAILURE || result.getStatus()==result.SKIP) {
			String screenshotPath = TestUtil.getScreenshot(getdriver(), result.getName());
			result.setAttribute("screenshotPath", screenshotPath); //sets the value the variable/attribute screenshotPath as the path of the screenshot
		}
		getdriver().quit();
		unload();
	}

}
