package com.qa.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.qa.base.TestBase;

public class SelectCompanyPage extends TestBase{
	
	//Initializing the Page Objects:
	
		@FindBy(xpath = "//body/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]")
		WebElement companySelectionText;
		
		@FindBy(xpath="//body/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/span[1]/i[1]")
		WebElement menuBar;
		
		@FindBy(xpath ="//span[contains(text(),'Show Active Client Only')]")
		WebElement showActiveClient;
		
		@FindBy(xpath="//span[contains(text(),'Include Inactive Companies')]")
		WebElement includeInactveCompanies;
		
		@FindBy(xpath = "//a[@class ='chosen-single']/span")
		WebElement selectCompanyDrop;
		
		@FindBy(xpath = "//*[@id=\"drpCompanyID_chosen\"]/div/div/input")
		WebElement searchBox;
		
		@FindBy(xpath = "//button[contains(text(),'Select')]")
		WebElement selectBtn;
		
		@FindBy(xpath = "//a[@id='lnkCreateCompany']")
		WebElement createNewCmpny;
		
		//Create New Company Modal
		@FindBy(xpath="//h4[contains(text(),'Create Company')]")
		WebElement createCompanyText;
		
		@FindBy(xpath="//input[@id='CompanyName']")
		WebElement companyNameTextBox;
		
		//Initialize Settings for the Company Creation
		@FindBy(xpath="//label[contains(text(),'Initialize Settings From')]")
		WebElement initSettingsTxt;
		
		@FindBy(xpath="//select[@id='ddlCompanies']")
		WebElement cmpnyTemplDrop;
		
		@FindBy(xpath="//label[contains(text(),'Copy System Setup')]")
		WebElement cpySysSetup;
		
		@FindBy(xpath="//input[@id='cbCopySystemSetup']")
		WebElement cpySysSetupChbx;
		
		@FindBy(xpath="//label[contains(text(),'Copy Themes')]")
		WebElement copyThemes;
		
		@FindBy(xpath="//input[@id='cbCopyThemes']")
		WebElement copyThemesChbx;
		
		@FindBy(xpath="//label[contains(text(),'Copy NLC DB')]")
		WebElement cpyNLCDB;
		
		@FindBy(xpath="//input[@id='cbCopyNLCDB']")
		WebElement cpyNLCDBChbx;
		
		@FindBy(xpath="//input[@id='btnSave']")
		WebElement createCompanyBtn;
		
		
		@FindBy(xpath="//span[@id='lblError']")
		WebElement erroMsg;
		//Initializing the Page Factory/Objects:
		public SelectCompanyPage(){
			PageFactory.initElements(getdriver(), this); //"this" means current class object. All the above variables will be initialized with this driver
		}
		//Actions/Methods on this page:
		public String verifySelectCompanyPageTitle(){
			return getdriver().getCurrentUrl();
			 
		}
	/*	public boolean verifyCorrectUserNameByName(String name){
			//return userNameLabel.isDisplayed();
			return getdriver().findElement(By.xpath("//*[@id=\"tblLoggedUserDetail\"]/tbody/tr[1]/td[2]/div and text()='"+name+"'")).isDisplayed();
		}*/
		
		public boolean cmpynSelectionLabel() {
			return companySelectionText.isDisplayed();
		}
		public boolean cmpynSelectionMenuBar() {
			return menuBar.isDisplayed();
		}
		//click on the Select Company Menu Bar
		public void clickMenuBar() {
			menuBar.click();
		}
		public boolean showActiveCompanies() {
			return showActiveClient.isDisplayed();
		}
		public boolean includeInactiveCompanies() {
			return includeInactveCompanies.isDisplayed();
		}
		public boolean selectCompanyDrop() {
			return selectCompanyDrop.isDisplayed();
		}
		public boolean selectCompanyBtn() {
			return selectBtn.isDisplayed();
		}
		
		//methods of create new Company link
		public boolean createNewCmpnyLink() {
			return createNewCmpny.isDisplayed();
		}
		public void clickCreateNewcompany() {
			createNewCmpny.click();
		}
		public boolean createCompanyTextMsg() {
			return createCompanyText.isDisplayed();
		}
		
		
		public void autoSelectCompany() {
			 selectCompanyDrop.click();	
		}
		public void searchSelectCompany(String search) {
			searchBox.sendKeys(search);
		}
		public void selectCompanyOption() {
			searchBox.sendKeys(Keys.ARROW_DOWN);
			searchBox.sendKeys(Keys.ENTER);
			
		}
		public void selectButton() {
			selectBtn.submit();
		}
		
		//Methods Initialize Settings for the Company Creation
		public boolean companyNameTextBox() {
			return companyNameTextBox.isDisplayed();
		}
		public String InitializeSettingsText() {
			 return initSettingsTxt.getText();
		}
		public boolean cpySyssetupText() {
			return cpySysSetup.isDisplayed();
		}
		public boolean cpyThemesText() {
			return copyThemes.isDisplayed();
		}
		public boolean cpyNLCDBText() {
			return cpyNLCDB.isDisplayed();
		}
		
		public void setupsCheckbox() {
			cpySysSetupChbx.isSelected();
			copyThemesChbx.isSelected();
			cpyNLCDBChbx.isSelected();
		}
		public void setupsCheckboxDisabled() {
			cpySysSetupChbx.isEnabled();
			copyThemesChbx.isEnabled();
			cpyNLCDBChbx.isEnabled();
		}
		public boolean createCompanyBtnTxt() {
			return createCompanyBtn.isDisplayed();
		}
		public void createCompanyBtn() {
			 createCompanyBtn.click();
		}
		
		public String selectCompanytemplate() {
			WebElement dropdown = cmpnyTemplDrop;
			Select opt = new Select(dropdown);
			WebElement deaultoption = opt.getFirstSelectedOption();
			return deaultoption.getText();
		}
		
		public String companyError() {
			return erroMsg.getText();
		}
		
		

}
