/**
 * 
 */
package com.qa.pages;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.qa.base.TestBase;
import com.qa.util.TestUtil;

/**
 * @author Amit Kumar
 *
 */
public class SatisfiInboxPage extends TestBase {
	@FindBy(xpath = "//div[contains(text(),'Inbox')]")
	WebElement header;

	@FindBy(xpath = "//i[@id='showNotification']")
	WebElement notificatioBell;

	@FindBy(xpath = "//input[@placeholder='Search']")
	WebElement searchBar;

	@FindBy(xpath = "//button[@id='searchDropdownBtn']//span")
	WebElement searchDrop;

	@FindBy(xpath = "//button[@id='searchDropdownBtn']")
	WebElement srchDrop;
	
	@FindAll(@FindBy(xpath = "//button[@id='searchDropdownBtn']//li"))
	List<WebElement> srchDroplist;

	@FindBy(xpath = "//button[contains(text(),'Mark Closed')]")
	WebElement markClosed;

	@FindBy(xpath = "//button[contains(text(),'Mark Read')]")
	WebElement markRead;
	
	@FindBy(xpath="//body/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/button[1]")
	WebElement more;
	
	@FindBy(xpath="//button[contains(@data-bind,'click: BroadcastIconClick')]")
	WebElement broadcast;

	@FindBy(xpath="//button[contains(@data-bind,'click: FilterIconClick')]")
	WebElement filter;
	
	
	//List of Messages Columns
	@FindBy(xpath="//th[contains(text(),'Latest/Initial Time')]")
	WebElement latInitialTime;
	
	@FindBy(xpath="//th[contains(text(),'Details')]")
	WebElement detailsCol;
	
	@FindBy(xpath="//th[contains(text(),'Status/Last Action')]")
	WebElement statslastActin;
	
	@FindBy(xpath="//nobr[contains(text(),'Customer Code/Submission Code')]")
	WebElement custCodeSubCode;
	
	@FindBy(className = "chkAllStatus")
	WebElement selectAll;
	
	//Locators for Advanced Search UI Elements
	
	@FindBy(xpath="//li[contains(text(),'<Advanced Search>')]")
	WebElement advancedSrch;
	
	@FindBy(xpath="//h4[contains(text(),'Advanced Search')]")
	WebElement advSearchHead;
	
	@FindBy(xpath="//input[@id='txtAdvancedSearchString']")
	WebElement search;
	
	@FindBy(xpath="//input[@id='chkIsAdvSearchChatMsg']")
	WebElement searchChatMsg;
	
	@FindBy(xpath="//label[contains(text(),'Status')]")
	WebElement statusTxt;
	
	@FindBy(xpath="//select[@id='AdvSearchStatus']")
	WebElement statusDrop;
	
	@FindBy(xpath="//label[contains(text(),'Flags')]")
	WebElement flagsTxt;
	
	@FindBy(xpath="//*[@id=\"AdvSearchFlags\"]/div/i[1]")
	WebElement upFlag;
	
	@FindBy(xpath="//*[@id=\"AdvSearchFlags\"]/div/i[2]")
	WebElement downFlag;
	
	@FindBy(xpath="//*[@id=\"AdvSearchFlags\"]/div/i[3]")
	WebElement flag;
	
	@FindBy(xpath="//*[@id=\"AdvSearchFlags\"]/div/i[4]")
	WebElement starFlag;
	
	@FindBy(xpath="//select[@id='AdvSearchPage']")
	WebElement pageDrop;
	
	@FindBy(xpath="//input[@id='txtAdvSearchCustomerCode']")
	WebElement custCode;
	
	@FindBy(xpath="//input[@id='txtAdvSearchSubmissionCode']")
	WebElement subCode;
	
	@FindBy(xpath="//input[@id='AdvSearchCustomerID']")
	WebElement custID;
	
	@FindBy(xpath="//input[@id='AdvSearchSubmissionID']")
	WebElement submID;
	
	@FindBy(xpath="//input[@id='AdvSearchClientId']")
	WebElement clID;
	
	@FindBy(xpath="//input[@id='AdvSearchFormDate']")
	WebElement fromDate;
	
	@FindBy(xpath="//select[@id='AdvSearchDateList']")
	WebElement toDateList;
	
	@FindBy(xpath="//button[contains(text(),'Search')]")
	WebElement searchBtn;
	
	@FindBy(xpath="//*[@id=\"divAdvancedSearch\"]/div[2]/div/div[3]/button[2]")
	WebElement cancelBtn;
	
	//Cross button
	@FindBy(xpath="//i[contains(@data-bind,'click: SearchRowCloseIconClick')]")
	WebElement searchResultsClose;
	
	@FindAll(@FindBy(xpath="//div[contains(@data-bind,'text: SubmissionStatus()')]"))
	List<WebElement> searchRes;
	
	@FindAll(@FindBy(xpath="//div[contains(@data-bind,'text: LastUpdateFormatted')]"))
	List<WebElement> lastupdDateRes;
	
	
	//Realtime Filters locators
	@FindBy(xpath="//h4[contains(text(),'Realtime Filters')]")
	WebElement realtimeHeader;
	
	@FindBy(xpath ="//input[@id='chkSubmissionFilterToday']")
	WebElement todayOnly;
	
	@FindBy(xpath ="//input[@id='chkSubmissionFilterClosed']")
	WebElement includeClosed;
	
	@FindBy(xpath ="//input[@id='chkSubmissionFilterUnread']")
	WebElement unreadOnly;
	
	@FindBy(xpath ="//input[@id='chkSubmissionFilterFlag']")
	WebElement hasFlag;
	
	@FindBy(xpath ="//input[@id='chkSubmissionFilterTestSubs']")
	WebElement hideTestSub;
	
	@FindBy(xpath ="//input[@id='chkSubmissionFilterSubsPages']")
	WebElement selectPages;
	
	@FindBy(xpath="//button[contains(text(),'Reset')]")
	WebElement resetbtnfilter;
	
	@FindBy(xpath="//button[contains(text(),'Apply')]")
	WebElement applybtnfilter;
	
	@FindBy(xpath="//button[contains(text(),'Cancel')]")
	WebElement cancelbtnfilter;
	
	@FindBy(xpath="//*[@id=\"divRealTimeFilters\"]/div[2]/div/div[1]/button/span[1]")
	WebElement closerealfilter;
	
	@FindBy(xpath="//input[contains(@value,'3983')]")
	WebElement amitPage;
	
	@FindBy(xpath="//input[contains(@value,'3977')]")
	WebElement adminPage;
	
	//Inbox Message Validation locators
	@FindAll(@FindBy(xpath="//input[contains(@data-bind,'attr: { SubmissionId: SubmissionInboxId')]"))
	List<WebElement> msgCheckbox;
	
	@FindBy(xpath="//input[contains(@submissionid,'136313']")
	WebElement amitMsgChk;
	// Initializing the Page Factory/Objects:
	public SatisfiInboxPage() {
		PageFactory.initElements(getdriver(), this); // "this" means current class object. All the above variables will
														// be initialized with this driver
	}

	// Actions/Methods on this page:
	public String verifyInboxPageTitle() {
		return getdriver().getCurrentUrl();

	}

	public String inboxHeader() {
		return header.getText();
	}
	
	public boolean searchDropBar() {
		return srchDrop.isDisplayed();
	}
	
	public boolean markClosedDisp() {
		return markClosed.isDisplayed();
	}
	public boolean markReadDisp() {
		return markRead.isDisplayed();
	}
	public boolean moreDisp() {
		return more.isDisplayed();
	}
	
	public boolean broadcastDisp() {
		return broadcast.isDisplayed();
	}
	public boolean filterDisp() {
		return filter.isDisplayed();
	}
	public void markButtonDisabled() {
		markClosed.isEnabled();
		markRead.isEnabled();
		more.isEnabled();
	}
	public boolean latInitialTimeDis() {
		return latInitialTime.isDisplayed();
	}
	public boolean detailsColDis() {
		return detailsCol.isDisplayed();
	}
	public boolean statslastActinDis() {
		return statslastActin.isDisplayed();
	}
	public boolean custCodeSubCodeDis() {
		return custCodeSubCode.isDisplayed();
	}
	public boolean notificationBell() {
		return notificatioBell.isDisplayed();
	}
	public boolean selectAllDis() {
		return selectAll.isDisplayed();
	}
	public void searchDropOptions() {
		List<String> actualList = new ArrayList<String>();
		
		for (WebElement element : srchDroplist) {
			actualList.add(element.getText());
		}

		List<String> expectedList = new ArrayList<String>();
		expectedList.add("TODAY");
		expectedList.add("YESTERDAY");
		expectedList.add("HAS FLAG");
		expectedList.add("UNREAD SUBMISSIONS");
		expectedList.add("OPEN SUBMISSIONS");
		expectedList.add("CLOSED SUBMISSIONS");
		expectedList.add("<Advanced Search>");
		
		for (int i = 0; i < actualList.size(); i++) {
			System.out.println("Actual Options:" + actualList.get(i) + "& Expected :" + expectedList.get(i));
			Assert.assertTrue(actualList.get(i).equals(expectedList.get(i)));
			//Assert.assertEquals(actualList.get(i), expectedList.get(i));
		
		}

	}
	public void searchDropClick() {
		searchDrop.click();
	}
	public void searchOptions(String search) {
		searchBar.sendKeys(search);
		searchBar.sendKeys(Keys.ENTER);
	}
	
	public String advancedSearchHeader() {
		return advSearchHead.getText();
	}
	
	public void searchFieldVal() {
		search.clear();
		search.isDisplayed();
		search.isEnabled();
	}
	public void searchChatMessage() {
		searchChatMsg.isEnabled();
		searchChatMsg.click();
	}
	public void flagsDisplay() {
		flagsTxt.isDisplayed();
		upFlag.isDisplayed();
		downFlag.isDisplayed();
		flag.isDisplayed();
		starFlag.isDisplayed();
	}
	public void statusOptions() {
		
		statusTxt.isDisplayed(); 		//Status Text is Displaying
		statusDrop.click();
		
		WebElement pageopt = statusDrop;
		Select option = new Select(pageopt);
		List<String> actualList = new ArrayList<String>();
		for (WebElement element : option.getOptions()) {
			actualList.add(element.getText());
		}

		List<String> expectedList = new ArrayList<String>();
		expectedList.add("ALL");
		expectedList.add("OPEN");
		expectedList.add("CLOSED");
		for (int i = 0; i < actualList.size(); i++) {
			System.out.println("Actual Options:" + actualList.get(i) + "& Expected :" + expectedList.get(i));
			Assert.assertTrue(actualList.get(i).equals(expectedList.get(i)));
			//Assert.assertEquals(actualList.get(i), expectedList.get(i));
		
		}
	}
	
	/*public void pageOptions() {
		WebElement pageopt = pageDrop;
		Select option = new Select(pageopt);
		List<String> actualList = new ArrayList<String>();
		for (WebElement element : option.getOptions()) {
			actualList.add(element.getText());
		}

		List<String> expectedList = new ArrayList<String>();
		expectedList.add("TODAY");
		expectedList.add("YESTERDAY");
		expectedList.add("HAS FLAG");
		expectedList.add("UNREAD SUBMISSIONS");
		expectedList.add("OPEN SUBMISSIONS");
		expectedList.add("CLOSED SUBMISSIONS");
		expectedList.add("<Advanced Search>");
		
		for (int i = 0; i < actualList.size(); i++) {
			System.out.println("Actual Options:" + actualList.get(i) + "& Expected :" + expectedList.get(i));
			Assert.assertTrue(actualList.get(i).equals(expectedList.get(i)));
			//Assert.assertEquals(actualList.get(i), expectedList.get(i));
		
		}

	}*/
	public void customerCode() {
		custCode.isDisplayed();
		custCode.isEnabled();
		custCode.clear();	
	}
	public void submissionCode() {
		subCode.isDisplayed();
		subCode.isEnabled();
		subCode.clear();	
	}
	public void customerID() {
		custID.isDisplayed();
		custID.isEnabled();
		custID.clear();	
	}
	public void submissionID() {
		submID.isDisplayed();
		submID.isEnabled();
		submID.clear();	
	}
	public void clientID() {
		clID.isDisplayed();
		clID.isEnabled();
		clID.clear();	
	}
	public void dates() {
		fromDate.isDisplayed();
		toDateList.isDisplayed();		
	}
	public void advSrchuttons() {
		searchBtn.isDisplayed();
		cancelBtn.isDisplayed();
	}
	public void advancedSearchSel() {
		searchDrop.click();
		advancedSrch.click();
	}
	public void advSearchClose() {
		searchResultsClose.click();
	}
	
	public void filteredSearch() {
			//WebElement stsDrp = statusDrop;
			TestUtil.selectOptionFromDropdown(statusDrop,"CLOSED");
			WebElement dateOptions = toDateList;
			TestUtil.selectOptionFromDropdown(dateOptions,"Include Previous 30 Days");
			
	}
	
	public void matchingResults() {
						
		List<String> actualList = new ArrayList<String>();
		for (WebElement element : searchRes) {
			
			actualList.add(element.getText());
		}	
		
		List<String> expectedList = new ArrayList<String>();
		for(int a = 0; a < actualList.size();a++) 
		{
			expectedList.add("CLOSED");
		}
		
		for (int i = 0; i < actualList.size(); i++) {
			System.out.println("Actual Options:" + actualList.get(i) + " & Expected :" + expectedList.get(i));
			Assert.assertTrue(actualList.get(i).equals(expectedList.get(i)));
			//Assert.assertEquals(actualList.get(i), expectedList.get(i));
		
		}
	}
	/*public void matchingDate() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String fdate = LocalDate.now().minusDays(30).format(formatter);
		
		List<String> actualDate = new ArrayList<String>();
		
		for(WebElement eledate : lastupdDateRes) {
				actualDate.add(eledate.getText().split(" "));
		}
		
		for (int i = 0; i < actualDate.size(); i++) {
			System.out.println("Actual Options:" + actualDate.get(i) + " & Expected :" + fdate);
			Assert.assertTrue(actualDate.get(i).equals(fdate));
		
		}
	}*/
	
	public void searchBtnClick() {
		searchBtn.click();
	}
	public void cancelBtnClick() {
		cancelBtn.click();
	}
	
	public void inboxfilters() {
	filter.click();
	}
	public String relatimeFilterHead() {
		return realtimeHeader.getText();
	}
	
	public void realTimefiltersUI() {
		todayOnly.isDisplayed();
		todayOnly.isEnabled();
		includeClosed.isDisplayed();
		includeClosed.isEnabled();
		unreadOnly.isDisplayed();
		unreadOnly.isEnabled();
		hasFlag.isDisplayed();
		hasFlag.isEnabled();
		hideTestSub.isDisplayed();
		hideTestSub.isEnabled();
		selectPages.isDisplayed();
		selectPages.isEnabled();
		resetbtnfilter.isDisplayed();
		applybtnfilter.isDisplayed();
		cancelbtnfilter.isDisplayed();
		closerealfilter.isDisplayed();
	}
	public void todayOnlyFilter() {
		todayOnly.click();
		todayOnly.isSelected();
	}
	public void includeClosedFilter() {
		includeClosed.click();
		includeClosed.isSelected();

	}
	public void unreadOnlyFilter() {
		unreadOnly.click();
		unreadOnly.isSelected();

	}
	public void hasFlagFilter() {
		hasFlag.click();
		hasFlag.isSelected();

	}
	public void hideTestSubmissionFilter() {
		hideTestSub.click();
		hideTestSub.isSelected();
	}
	public void selectPagesFilter() {
		selectPages.click();
		selectPages.isSelected();
	}
	public void clickApplyBtn() {
		applybtnfilter.click();
	}
	public void cancelRealtimeClick() {
		cancelbtnfilter.click();
	}
	public void clickPagesSelect() {
		adminPage.click();
		amitPage.click();
	}
	public void selectPageOpenMatRes() {
		
		List<String> actualList = new ArrayList<String>();
		for (WebElement element : searchRes) {
			
			actualList.add(element.getText());
		}	
		
		List<String> expectedList = new ArrayList<String>();
		for(int a = 0; a < actualList.size();a++) 
		{
			expectedList.add("OPEN");
		}
		
		for (int i = 0; i < actualList.size(); i++) {
			System.out.println("Actual Options:" + actualList.get(i) + " & Expected :" + expectedList.get(i));
			Assert.assertTrue(actualList.get(i).equals(expectedList.get(i)));
			//Assert.assertEquals(actualList.get(i), expectedList.get(i));
		
		}
	}
	public void checkMsgBox() {
		
		
		for(WebElement element : msgCheckbox)
		{
			element.click();
			if(element.isSelected()) {
				markClosed.isEnabled();
				markRead.isEnabled();
				more.isEnabled();
				Assert.assertTrue(true);
			}
			else {
				Assert.assertTrue(false);
			}
		}
		
	}
	public void amitMsgCheckbox() {
		amitMsgChk.click();
		
		if(amitMsgChk.isSelected()) {
			markClosed.isEnabled();
			markRead.isEnabled();
			more.isEnabled();
			Assert.assertTrue(true);
		}
		else {
			Assert.assertTrue(false);
		}
	}
	
}
