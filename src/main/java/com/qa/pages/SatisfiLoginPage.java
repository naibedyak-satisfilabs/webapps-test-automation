package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.qa.base.TestBase;

public class SatisfiLoginPage extends TestBase {

	// Page Factory - OR:
	@FindBy(xpath = "//input[@id='EmailUser']")
	WebElement username;

	@FindBy(xpath = "//input[@id='Password']")
	WebElement password;

	@FindBy(xpath = "//button[contains(text(),'Login')]")
	WebElement loginBtn;

	@FindBy(xpath = "//p[contains(text(),'Please enter your e-mail address or username and password')]")
	WebElement message;
	
	@FindBy(xpath = "//div[contains(text(),'© 2022 Satisfi Labs Inc. All rights reserved.')]")
	WebElement copyrightmessage;
	
	@FindBy(xpath = "//*[@id=\"LoginForm\"]/form/div[6]/ul/li")
	WebElement invalidmessage;
	
	@FindBy(xpath = "//img[contains(@src,'solid.svg')]")
	WebElement SatisfiLogo;
	
	@FindBy(xpath = "//span[contains(text(),'E-Mail or Username is required')]")
	WebElement emailReqError;
	
	@FindBy(xpath = "//span[contains(text(),'Password is required')]")  
	WebElement passwordReqError;
	
	
	//Forgot Password modal
	@FindBy(id= "hlForgotPassword")
	WebElement forgotPassword;
	
	@FindBy(xpath ="//span[contains(text(),'×')]")
	WebElement closeFrgtPswd;
	
	@FindBy(xpath ="//body/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]")
	WebElement frgtEmailText;
	
	@FindBy(id ="txtUserForgotEmail")
	WebElement inpemailTextFrgt;
	
	@FindBy(id ="btnPasswordReset")
	WebElement rqstPasswordReset;
	
	@FindBy(xpath ="//div[@id='validForgotPassMessage']")
	WebElement invlidEmailAdd;
	
	@FindBy(xpath="//p[@id='paraUserPasswordResetErrorMsg']")
	WebElement noUserFound;
	
	//Reset Link Sent Message
	@FindBy(xpath ="//span[@id='spnResetLinkSentMsg']")
	WebElement resetLink;
	
	@FindBy(xpath ="//li[contains(text(),'This account is locked due to many failed login at')]")
	WebElement accLocked;
	
	//Logout Elements
	@FindBy(xpath ="//div[contains(text(),'Test Server')]")
	WebElement hoverlogoutuser;
	
	@FindBy(xpath="//body/div[1]/div[1]/div[1]/div[2]/ul[1]/li[2]/ul[1]/li[3]/a[1]")
	WebElement logoutuser;
	//Initializing the Page Factory/Objects:
		public SatisfiLoginPage(){
			PageFactory.initElements(getdriver(), this); //"this" means current class object. All the above variables will be initialized with this driver
		}
	
	
	/** Actions/Methods on Login page: **/

	public String validateLoginPageTitle() { // this method will return a string(getTitle())
		return getdriver().getTitle();
	}	
	public boolean validateInfoMessage() {
		return message.isDisplayed(); // to validate if login message is displayed
	}
	public boolean validateCopyrightMessage() {
		return copyrightmessage.isDisplayed();
	}
	
	public String validateInvalidMessage() {
		return invalidmessage.getText();
	}
	
	public String validateInvalidEmailMessage() {
		return emailReqError.getText();
	}
	
	public String validateInvalidPswdMessage() {
		return passwordReqError.getText();
	}
	public boolean validateSatisfiImage() { // isDisplayed() method will return true/false. So, this method will return
											// boolean
		return SatisfiLogo.isDisplayed();
	}
	public boolean validateForgotPassword() {
		return forgotPassword.isDisplayed();
	}
	//methods for checking forgot Password 
	public void clickForgotPwd() {
		forgotPassword.click();
	}
	
	public boolean forgotPswdResetEmail() { 
		return inpemailTextFrgt.isDisplayed();	
	}
	public boolean frgtPswdResetEmailText() {
		return frgtEmailText.isDisplayed();
	}
	public boolean frgtPswdResetBtn() { 
		return rqstPasswordReset.isDisplayed();	
	}
	public boolean closeFrgtPswd() {
		return closeFrgtPswd.isDisplayed();
	}
	public void closeFrgtPswdModal() {
		
		closeFrgtPswd.click();
	}
	public String invalidEmailAddress() {
		return invlidEmailAdd.getText();
	}
	public void restEmailAdd(String emailadd) {
		inpemailTextFrgt.sendKeys(emailadd);
		rqstPasswordReset.click();
	}
	public String noUserFoundMsg() {
		return noUserFound.getText();
	}
	
	public String userResetLink() {
		return resetLink.getText();
	}
	
	public String userAccLocked() {
		return accLocked.getText();
	}
	
	public void invalidLogin(String usrname, String pswd) {
		username.sendKeys(usrname);
		password.sendKeys(pswd);
		loginBtn.click();
		
	}
	public void checkMaxValue(String usernm, String passwd) {
		username.sendKeys(usernm);
		String max_usernm =username.getAttribute("value");
		int usernm_size =max_usernm.length();
		
		if(usernm_size < 500) {			//Validate if the username field accepts less than 500 characters
			Assert.assertTrue(true);
			System.out.println(usernm_size);
		}else {
			Assert.assertTrue(false);
			System.out.println(usernm_size);
		}
		password.getAttribute("value");
		String max_passwd = password.getAttribute("value");
		int pass_size = max_passwd.length();
		
		if(pass_size < 100) {			//Validate if the password field accepts less than 100 characters
			Assert.assertTrue(false);
			System.out.println(pass_size);
		}else {
			Assert.assertTrue(true);
			System.out.println(pass_size);
		}
		loginBtn.click();
	}
	
	public  SelectCompanyPage login(String uname, String pwd) { // return type of login() method is HomePage
		username.sendKeys(uname);
		password.sendKeys(pwd);
		loginBtn.click();
		return new SelectCompanyPage(); // This login method should return SelectCompanyPage Page class object.
	}
	
	
	/** Actions/Methods on Logout : **/
	public void logout() {
		Actions action = new Actions(getdriver());
		action.moveToElement(hoverlogoutuser);
		action.moveToElement(logoutuser).click();
		action.build().perform();
	}
	

}
