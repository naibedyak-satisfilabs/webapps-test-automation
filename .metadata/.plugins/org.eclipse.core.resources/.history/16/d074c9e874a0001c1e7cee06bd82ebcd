package com.qa.testcases;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pages.SatisfiLoginPage;
import com.qa.pages.SelectCompanyPage;
import com.qa.util.TestUtil;

public class SatisfiLoginPageTest extends TestBase {

	SatisfiLoginPage loginPage; // Login page object reference. It will be defined at class level to be used
								// throughout the program.
	SelectCompanyPage selectCompanyPage; // dashboardPage is DashboardPage reference variable or DashboardPage page
											// object reference
	String sheetName = "Sheet1";
	String sheetName1 = "Sheet2";

	//String randPass = TestUtil.generateRandomGenerator();
	// Create a constructor of LoginPageTest class
	public SatisfiLoginPageTest() {
		super();
	}

	@Parameters({ "browser", "appEnvironment" })
	@BeforeMethod(alwaysRun = true)
	public void setUp(String Browser, String Env) {
		reportLog("Login Page class - Before method execution");
		initialization(Browser, Env);
		loginPage = new SatisfiLoginPage(); // Create object of SatisfiLoginPage() class. This is used to access all the
											// functions/methods of SatisfiLoginPage() class
	}

	@Test(priority = 1, groups = { "PRIME-T209" }, description = "TC_Login_01")
	public void login_Page_Title_Test() throws Exception {
		reportLog("Validating the titie of the Login Page");
		String title = loginPage.validateLoginPageTitle(); // this will return title in string.
		Assert.assertEquals(title, "Login - Satisfi"); // assert from testNG is used to verify or validate
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Verify Login Page Title");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);

	}

	@Test(priority = 2, groups = { "PRIME-T209" }, description = "TC_Login_02")
	public void satisfi_LoginInfo_UI_Elements_Test() {
		reportLog("Validating the various web elements present in login screen");
		boolean msg = loginPage.validateInfoMessage();
		Assert.assertTrue(msg);
		reportLog("Validated the Information Message is displaying in login screen");
		
		boolean flag = loginPage.validateSatisfiImage();
		Assert.assertTrue(flag);
		reportLog("Validated the Satisfi Logo is displaying in login screen");
		
		boolean cpymsg = loginPage.validateCopyrightMessage();
		Assert.assertTrue(cpymsg);
		reportLog("Validated the Copyright Message is displaying in login screen");
		
		boolean fgtPwd = loginPage.validateForgotPassword();
		Assert.assertTrue(fgtPwd);
		reportLog("Validated the Forgot Password Link is displaying in login screen");	
	}
	
	@DataProvider
	public Object[][] getTestData() throws InvalidFormatException {
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}

	// Data Driven Test Case for login of each type of user.
	@Test(priority = 3, groups = { "PRIME-T209" }, description = "TC_Login_03", dataProvider = "getTestData")
	public void login_DD_Test(String userName, String password) throws Exception {
		reportLog("Login to Satisfi Labs app");

		// this login() method is returning an object of HomePage class. So I stored
		// this in dashboardPage class reference object
		reportLog("Login using username as: " + userName + " and password as: " + password);

		// Data Driven example for login function
		selectCompanyPage = loginPage.login(userName, password);

		// Fetching data from properties file
		// selectCompanyPage = loginPage.login(prop.getProperty("username"),
		// prop.getProperty("password"));

		reportLog("Validating login is successfull by checking dashboard page title");
		String companyPageTitle = selectCompanyPage.verifySelectCompanyPageTitle();
		String CompanySelectPage1 = "https://dashboard.satisfitest.us/";
		Assert.assertEquals(companyPageTitle, CompanySelectPage1,"Select Company Title Not Matched");
		//Sample for color coding the log line
		reportLog("<b><font color='Purple'> Login Verification successfully Completed.:</font><b>");
		
		reportLog("*****Selecting Company*****");
		//TestUtil.threadMethod();
		TestUtil.threadMethod();
		reportLog("Clicking on the Select Company Dropdown");
		selectCompanyPage.autoSelectCompany();
		reportLog("Searching the Mindfire QA Test Server 959 Company:");
		selectCompanyPage.searchSelectCompany("959");
		
		selectCompanyPage.selectCompanyOption();
		TestUtil.threadMethod();
		reportLog("Validated and Selected the Company and Clicking on Select Button");
		selectCompanyPage.selectButton();
		
		reportLog("Company selected");
		
		reportLog("<b><font color='Purple'> Login Verification successfully Completed, User Navigated to Inbox Page.</font><b>");
		//Sample code on how to take screenshot at a particular step that will be added in Extent report.
		
		reportLog("Logging out of the application.");
		loginPage.logout();
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	@Test(priority=4, groups = {"PRIME-T209"}, description="TC_Login_04")
	public void login_Invalid_Message_Test() throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Validating Invalid login by giving wrong credentials");
		loginPage.invalidLogin("Amit@gmail.com","Password");
		String invalidinfo = loginPage.validateInvalidMessage();
		String actualInvalidInfo = "Invalid email/password combination";
		Assert.assertEquals(invalidinfo,actualInvalidInfo);
		reportLog("<b><font color='Purple'>Validated Invalid login by giving wrong credentials</font><b> "+invalidinfo);
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority=5, groups = {"PRIME-T209"}, description="TC_Login_05")
	public void login_Invalid_Email_Test() throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Validating Invalid Username by giving wrong credentials");
		loginPage.invalidLogin(" ","Password");
		String invalidEmailinfo = loginPage.validateInvalidEmailMessage();
		String actualInvalidInfo = "E-Mail or Username is required.";
		Assert.assertEquals(invalidEmailinfo,actualInvalidInfo);
		reportLog("<b><font color='Purple'>Validated the Invalid </font><b> "+invalidEmailinfo+" message in Login Screen.");
		
		reportLog("Taking Screenshot of the failed Login below.");
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority=6, groups = {"PRIME-T209"}, description="TC_Login_06")
	public void login_Invalid_Password_Test() throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Validating Invalid Username by giving wrong credentials");
		loginPage.invalidLogin("Amit@gmail.com"," ");
		String invalidPswdinfo = loginPage.validateInvalidPswdMessage();
		String actualInvalidInfo = "Password is required.";
		Assert.assertEquals(invalidPswdinfo,actualInvalidInfo);
		reportLog("<b><font color='Purple'>Validated Invalid </font><b> "+invalidPswdinfo+" message in Login Screen");
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}

	@Test(priority = 7, groups = { "PRIME-T209" }, description = "TC_Login_07")
	public void login_Forgot_Password_Reset_Test() throws Exception {
		reportLog("Validating by Clicking the Forgot Password Link ");
		loginPage.clickForgotPwd();
		Thread.sleep(3000);
		
		reportLog("Validating the various web elements present in Forgot password Reset modal screen");
		boolean emailField = loginPage.forgotPswdResetEmail();
		Assert.assertTrue(emailField);
		reportLog("Validated the email-Field is displaying in Forgot Password Modal");
		
		boolean emailText = loginPage.frgtPswdResetEmailText();
		Assert.assertTrue(emailText);
		reportLog("Validated the email-Field Text is displaying in Forgot Password Modal");
		
		boolean frgtResetBtn = loginPage.frgtPswdResetBtn();
		Assert.assertTrue(frgtResetBtn);
		reportLog("Validated the Reset Button is displaying in Forgot Password Modal");
		
		boolean closeResetBtn = loginPage.closeFrgtPswd();
		Assert.assertTrue(closeResetBtn);
		reportLog("Validated the Close Button is displaying in Forgot Password Modal");
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority = 8, groups = { "PRIME-T209" }, description = "TC_Login_08")
	public void invalid_Email_Address_Test() throws Exception {
		reportLog("Validating by Clicking the Forgot Password Link ");
		loginPage.clickForgotPwd();
		TestUtil.threadMethod();		
		reportLog("Validating by providing Invalid Email Address in Forgot password Reset modal screen");
		loginPage.restEmailAdd("abc123@xys");
		TestUtil.threadMethod();
		reportLog("Clicking on Reset Button in Forgot password Reset modal screen");
		String invldEmailAdd = loginPage.invalidEmailAddress();
		String actualEmailMsg = "Invalid Email Address.";
		Assert.assertEquals(invldEmailAdd, actualEmailMsg);
		
		reportLog("<b><font color='Purple'>Validated </font><b> "+invldEmailAdd+" <b><font color='Purple'>message in Forgot password Modal</font><b>");
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority = 9, groups = { "PRIME-T209" }, description = "TC_Login_09")
	public void noUser_Found_Test() throws Exception  {
		reportLog("Validating by Clicking the Forgot Password Link ");
		loginPage.clickForgotPwd();
		TestUtil.threadMethod();		
		reportLog("Validating by providing Email Address in Forgot password Reset modal screen");
		loginPage.restEmailAdd("abc123@xys.com");
		TestUtil.threadMethod();
		reportLog("Clicking on Reset Button in Forgot password Reset modal screen");
		String noUserFnd = loginPage.noUserFoundMsg();
		//String actualEmailMsg = "No user found with the User ID or Email Address abc123@xys.com.";
		Assert.assertTrue(true, noUserFnd);
		reportLog("<b><font color='Purple'>Validated </font><b> "+noUserFnd+" <b><font color='Purple'>message in Forgot password Modal</font><b>");
		
		loginPage.closeFrgtPswdModal(); //To Close the Forgot Password Modal.
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@Test(priority = 10, groups = { "PRIME-T209" }, description = "TC_Login_10")
	public void User_Password_Reset_Test() throws Exception  {
		reportLog("Validating by Clicking the Forgot Password Link ");
		loginPage.clickForgotPwd();
		TestUtil.threadMethod();		
		reportLog("Validating by providing Email Address in Forgot password Reset modal screen");
		loginPage.restEmailAdd("amit.k@mindfiresolutions.com");
		TestUtil.threadMethod();
		reportLog("Clicking on Reset Button in Forgot password Reset modal screen");
		String userResetLink = loginPage.userResetLink();
		
		Assert.assertTrue(true, userResetLink);
		reportLog("<b><font color='Purple'>Validated </font><b> "+userResetLink+" <b><font color='Purple'>message in Forgot password Modal</font><b>");
		
		loginPage.closeFrgtPswdModal(); //To Close the Forgot Password Modal.
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	/*@DataProvider(name="data")
    public Object[][] getUserData(){
        return new Object[][] {   	
            {"amit.kumar1@mindfiresolutions.com", "jUgyjAn"},
            {"amit.kumar1@mindfiresolutions.com", "uvetahA"},
            {"amit.kumar1@mindfiresolutions.com", "utYmEqY"},
            {"amit.kumar1@mindfiresolutions.com", "utYmEsY"},
            {"amit.kumar1@mindfiresolutions.com", "utYmEeY"},
            {"amit.kumar1@mindfiresolutions.com", "utYmEtY"},
        };
                }*/
	
	@DataProvider
	public Object[][] getTestData1() throws InvalidFormatException {
		Object data[][] = TestUtil.getTestData(sheetName1);
		return data;
	}
	@Test(priority=11, groups = {"PRIME-T209"}, description="TC_Login_11",dataProvider="getTestData1")
	public void login_Locked_Password_Test(String username , String password) throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Validating by giving Username by giving Random Passwords ");
		
		reportLog("Login using username as: " + username + " and password as: " + password);

		// Data Driven example for login function
		selectCompanyPage = loginPage.login(username, password);
		
		TestUtil.threadMethod();
		
		String lockedUser = loginPage.userAccLocked();
		String actualuserAccLockedInfo = "This account is locked due to many failed login attempts. Please reset password or contact support.";
		Assert.assertEquals(lockedUser,actualuserAccLockedInfo);
		reportLog("<b><font color='Purple'>Validated Invalid </font><b> "+lockedUser+" message in Login Screen");
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@DataProvider(name="TestdataRestrict")
    public Object[][] getUserData(){
        return new Object[][] {   	
            {"amit.k@mindfiresolutions.com", "Password@12"}
        };
	}
	
	@Test(priority=12, groups = {"PRIME-T209"}, description="TC_Login_12",dataProvider="TestdataRestrict")
	public void user_Restricted_Test(String wusername , String wpassword) throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Validating ​User should get restricted to access the application  ");
		
		reportLog("Login using username as: " + wusername + " and password as: " + wpassword);

		// Data Driven example for login function
		selectCompanyPage = loginPage.login(wusername, wpassword);
		
		TestUtil.threadMethod();
		
		String invalidinfores = loginPage.validateInvalidMessage();
		String actualInvalidInfo = "Invalid email/password combination";
		Assert.assertEquals(invalidinfores,actualInvalidInfo);
		reportLog("<b><font color='Purple'>Validated Invalid login by giving wrong credentials</font><b> "+invalidinfores);
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Invalid Login after reset password Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@DataProvider(name="TestdataAllow")
    public Object[][] getUserData1(){
        return new Object[][] {   	
            {"amit.k@mindfiresolutions.com", "Password@97"}
        };
	}
	
	@Test(priority=13, groups = {"PRIME-T209"}, description="TC_Login_13",dataProvider="TestdataAllow")
	public void user_LoggedIn_Test(String rusername , String rpassword) throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Validating ​​User should be logged into the system succesfully after Resetting the password  ");
		
		reportLog("Login using username as: " + rusername + " and password as: " + rpassword);

		// Data Driven example for login function
		selectCompanyPage = loginPage.login(rusername, rpassword);
		TestUtil.threadMethod();
		
		reportLog("Validating login is successfull by checking dashboard page title");
		String companyPageTitle = selectCompanyPage.verifySelectCompanyPageTitle();
		String CompanySelectPage1 = "https://dashboard.satisfitest.us/";
		Assert.assertEquals(companyPageTitle, CompanySelectPage1,"Select Company Title Not Matched");
		//Sample for color coding the log line
		reportLog("<b><font color='Purple'> Login Verification successfully Completed.</font><b>");
		
		reportLog("*****Selecting Company*****");
		//TestUtil.threadMethod();
		TestUtil.threadMethod();
		reportLog("Clicking on the Select Company Dropdown");
		selectCompanyPage.autoSelectCompany();
		reportLog("Searching the Mindfire QA Test Server 959 Company:");
		selectCompanyPage.searchSelectCompany("959");
		
		selectCompanyPage.selectCompanyOption();
		TestUtil.threadMethod();
		reportLog("Validated and Selected the Company and Clicking on Select Button");
		selectCompanyPage.selectButton();
		
		reportLog("Company selected");
		
		reportLog("<b><font color='Purple'> Login Verification successfully Completed, User Navigated to Inbox Page.</font><b>");
		//Sample code on how to take screenshot at a particular step that will be added in Extent report.
		
		reportLog("Logging out of the application.");
		loginPage.logout();
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Login with New Password after Reset Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@DataProvider(name="TestdataMax")
    public Object[][] getUserData2(){
        return new Object[][] {   	
            {"qweyruiqweuriyquoweryquiowerqweruqopwueroiquwuiofposdiufaopsdfajklsdhfklahsdlfhkjashklf\r\n"
            		+ "qweyruiqweuriyquoweryquiowerqweruqopwueroiquwuiofposdiufaopsdfajklsdhfklahsdlfhkjashklfqweyruiqweuriyquoweryquiowerqweruqopwueroiquwuiofposdiufaopsdfajklsdhfklahsdlfhkjashklfqweyruiqweuriyquoweryquiowerqweruqopwueroiquwuiofposdiufaopsdfajklsdhfklahsdlfhkjashklfqweyruiqweuriyquoweryquiowerqweruqopwueroiquwuiofposdiufaopsdfajklsdhfklahsdlfhkjashklfqweyruiqweuriyquoweryquiowerqweruqopwueroiquwuiofposdiufaopsdfajk", "qweyruiqweuriyquoweryquiowerqweruqopwueroiquwuiofposdiufaopsdfajklsdhfklahsdlfhkjashklf\r\n"
            				+ "qweyruiqweuri"}
        };
	}
	
	@Test(priority=14, groups = {"PRIME-T209"}, description="TC_Login_14",dataProvider="TestdataMax")
	public void user_Pass_MaxValue_Test(String maxuser , String maxpass) throws Exception{
		reportLog("Login to Satisfi Labs app");
		reportLog("Validating ​User should get restricted to access the application  ");
		
		reportLog("Login using username as: " + maxuser + " and password as: " + maxpass);

		// Data Driven example for login function
		loginPage.checkMaxValue(maxuser, maxpass);
		
		TestUtil.threadMethod();
		
		String invalidinfores = loginPage.validateInvalidMessage();
		String actualInvalidInfo = "Invalid email/password combination";
		Assert.assertEquals(invalidinfores,actualInvalidInfo);
		reportLog("<b><font color='Purple'>Validated Invalid login by giving wrong credentials</font><b> "+invalidinfores);
		
		String screenshotPathAnyStep = TestUtil.getScreenshot(getdriver(), "Invalid Login after reset password Verified!!");
		takeScreenshotAtAnyStep(screenshotPathAnyStep);
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result) throws Exception {
		reportLog("Take sceenshot in case of failue and close browser");
		// Check if the test case failed or was skipped and take screenshot
		if (result.getStatus() == result.FAILURE || result.getStatus() == result.SKIP) {
			String screenshotPath = TestUtil.getScreenshot(getdriver(), result.getName());
			result.setAttribute("screenshotPath", screenshotPath); // sets the value the variable/attribute
																	// screenshotPath as the path of the screenshot
		}
		getdriver().quit();
		unload();
	}
}
