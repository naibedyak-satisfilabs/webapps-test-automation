package com.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;

import com.qa.util.TestUtil;
import com.qa.util.WebEventListener;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.Capabilities;
import java.time.Duration;

public class TestBase {

	//A static member is a member of a class that isn't associated with an instance of a class. Instead, the member belongs to the class itself.

	public static WebDriver driver; //Initialize webdriver with static variable. This is set to private because I am not using this anywhere outside this class
	public static Properties prop; //Public class variable for properties. This is a global variable(public static).I can use inside TestBase.java class and all other child classes also
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String browserName1;
	public static String browserVersion1;
	private static String propertiesFileName;
	public static String AppEnv;

	//private static final String KEY = "appEnvironment";

	//This is the logger class. Generate the logs? : use Apache log4j API (log4j jar)
	public static Logger log = Logger.getLogger(TestBase.class);

	//Method for adding logs passed from test cases. This is for extentTest HTML report and log4j log files as well
	public static void reportLog(String message) {
		log.info("Message: " + message);
		Reporter.log(message);
	}
	

	private static void selectOptionFromDropdown(WebElement ele, String value) 
	{
		Select drp = new Select(ele);
		List<WebElement> alloptions = drp.getOptions();
		
		for(WebElement option : alloptions)
		{
			if(option.getText().equals(value))
			{
				option.click();
				break;
			}
		}
		
	}
	
	
	//Method to take screenshot at any step you want
	public static void takeScreenshotAtAnyStep(String screenshotPath) {
		Reporter.log(screenshotPath);
	}

	//TestBase class public constructor.
	/*
	 * public TestBase(){ //Read the properties file try { prop = new Properties();
	 * //initializing the prop variable FileInputStream ip = new
	 * FileInputStream(System.getProperty("user.dir")+
	 * "//src//main//java//com//qa//config//config.properties"); prop.load(ip); }
	 * catch (FileNotFoundException e) { e.printStackTrace(); } catch (IOException
	 * e) { e.printStackTrace(); } }
	 */

	//To make driver thread safe. This will be required for parallel execution from testNG
	private static ThreadLocal<WebDriver> dr = new ThreadLocal<WebDriver>(); //Initialize thread local variable.

	//Getter method ThreadLocal variable dr. This is the driver instance, it will check which thread is calling this for parallel running.
	public static WebDriver getdriver() {
		return dr.get();
	}

	//Setter method. This will set the webdriver reference driverref
	public static void setDriver(WebDriver driverref) {
		dr.set(driverref); 
	}

	//For cleanup operation. This will remove any values stored by threadLocal variable. This will be used in afterclass method of each test.
	public static void unload() {
		dr.remove(); 
	}

	//Initialization method
	public static void initialization(String browserName, String env){

		//Read the properties file.Based on the parameter value provided for environment, the properties file will load
		try {
			if (env.trim().equalsIgnoreCase("Staging")) {
				propertiesFileName = "config_Staging.properties";
			} else if (env.trim().equalsIgnoreCase("Production")) {
				propertiesFileName = "config_Production.properties";
			}else if(env.trim().equalsIgnoreCase("Dev"))
			{
				propertiesFileName = "config_Dev.properties";
			}
			System.out.println("************"+env);
			prop = new Properties(); //initializing the prop variable
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+"//src//main//java//com//qa//config//"+propertiesFileName);
			prop.load(ip);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//String browserName = prop.getProperty("browser");

		if(browserName.trim().equalsIgnoreCase("chrome")){

			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//chromedriver.exe");
			//Create a map to store  preferences 
			Map<String, Object> prefs = new HashMap<String, Object>();

			//add key and value to map as follow to switch off browser notification
			//Pass the argument 1 to allow and 2 to block
			prefs.put("profile.default_content_setting_values.notifications", 2);
			// set ExperimentalOption - prefs 
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", prefs);
			
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver(options);
			setDriver(driver);
		}
		else if(browserName.trim().equalsIgnoreCase("firefox")){
			//System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"//geckodriver.exe");
			//Map<String, Object> prefs = new HashMap<String, Object>();

			//add key and value to map as follow to switch off browser notification
			//Pass the argument 1 to allow and 2 to block
			//prefs.put("profile.default_content_setting_values.notifications", 2);
			// set ExperimentalOption - prefs 
			//FirefoxOptions profile = new FirefoxOptions();
			
			//profile.addPreference("permissions.default.desktop-notification", 2);
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			setDriver(driver);
		}

		//To get Browser details:
		Capabilities browserCap = ((RemoteWebDriver) getdriver()).getCapabilities();
		browserName1 = browserCap.getBrowserName();
		browserVersion1 = browserCap.getBrowserVersion();

		//To get Application environment currently running
		
		//AppEnv = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter(KEY);
		AppEnv = prop.getProperty("environment");
		System.out.println(AppEnv);
		
		//To get the current time when 

		//Create EventFiringWebDriver class object
		e_driver = new EventFiringWebDriver(getdriver());
		// Now create object of EventListerHandler to register it with EventFiringWebDriver
		eventListener = new WebEventListener();
		//Register the eventListener class object with event firing webdriver object.
		e_driver.register(eventListener);
		//Assign this to the main driver
		//driver = e_driver;
		setDriver(e_driver);

		getdriver().manage().window().maximize();
		getdriver().manage().deleteAllCookies();
		getdriver().manage().timeouts().pageLoadTimeout(Duration.ofSeconds(TestUtil.PAGE_LOAD_TIMEOUT));
		getdriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(TestUtil.IMPLICIT_WAIT));

		//Open the url of the application
		getdriver().get(prop.getProperty("url"));

	}

}
